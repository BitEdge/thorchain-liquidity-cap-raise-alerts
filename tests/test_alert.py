from unittest.mock import call

from thorchain_liquidity_alerts.alert import send_alert


def test_send_alert_makes_voice_calls(mocker, monkeypatch):
    monkeypatch.setenv("TWILIO_ACCOUNT_SID", "twilio-sid-1")
    monkeypatch.setenv("TWILIO_AUTH_TOKEN", "twilio-auth-1")
    monkeypatch.setenv("TWILIO_FROM_NUMBER", "+123")
    monkeypatch.setenv("TWILIO_TO_NUMBERS", "+456,+789")
    TwilioClient = mocker.patch("thorchain_liquidity_alerts.alert.TwilioClient", autospec=True)
    send_alert("subject1", "message1")
    assert TwilioClient.call_args == call("twilio-sid-1", "twilio-auth-1")
    assert TwilioClient.return_value.calls.create.call_args_list == [
        call(
            twiml="<Response><Say>The thorchain liquidity cap has changed.</Say></Response>",
            from_="+123",
            to="+456",
        ),
        call(
            twiml="<Response><Say>The thorchain liquidity cap has changed.</Say></Response>",
            from_="+123",
            to="+789",
        ),
    ]
