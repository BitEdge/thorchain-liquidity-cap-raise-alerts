import pytest
import responses  # type: ignore

from thorchain_liquidity_alerts.thorchain import get_max_liquidity


@responses.activate
def test_get_max_liquidity():
    response_data = {
        "mimir//CHURNINTERVAL": 43200,
        "mimir//EMISSIONCURVE": 4,
        "mimir//FULLIMPLOSSPROTECTIONBLOCKS": 0,
        "mimir//FUNDMIGRATIONINTERVAL": 720,
        "mimir//MAXIMUMLIQUIDITYRUNE": 550000000000000,
        "mimir//MAXLIQUIDITYRUNE": 550000000000000,
        "mimir//MINIMUMBONDINRUNE": 30000000000000,
        "mimir//MINRUNEPOOLDEPTH": 1000000000000,
        "mimir//MINTSYNTHS": 1,
        "mimir//OBSERVATIONDELAYFLEXIBILITY": 20,
        "mimir//POOLCYCLE": 43200,
    }
    responses.add(
        responses.GET,
        "https://thornode.thorchain.info/thorchain/mimir",
        status=200,
        json=response_data,
    )
    assert get_max_liquidity() == 550000000000000


@responses.activate
@pytest.mark.parametrize("maximum_value, max_value", [(123, 456), (456, 123)])
def test_get_max_liquidity_returns_the_highest_of_MAXLIQUIDITYRUNE_and_MAXIMUMLIQUIDITYRUNE(
    maximum_value, max_value
):
    responses.add(
        responses.GET,
        "https://thornode.thorchain.info/thorchain/mimir",
        status=200,
        json={"mimir//MAXIMUMLIQUIDITYRUNE": maximum_value, "mimir//MAXLIQUIDITYRUNE": max_value},
    )
    assert get_max_liquidity() == 456


@responses.activate
def test_get_max_liquidity_returns_MAXLIQUIDITYRUNE_if_MAXIMUMLIQUIDITYRUNE_is_missing():
    responses.add(
        responses.GET,
        "https://thornode.thorchain.info/thorchain/mimir",
        status=200,
        json={"mimir//MAXLIQUIDITYRUNE": 123},
    )
    assert get_max_liquidity() == 123


@responses.activate
def test_get_max_liquidity_returns_MAXIMUMLIQUIDITYRUNE_if_MAXLIQUIDITYRUNE_is_missing():
    responses.add(
        responses.GET,
        "https://thornode.thorchain.info/thorchain/mimir",
        status=200,
        json={"mimir//MAXIMUMLIQUIDITYRUNE": 123},
    )
    assert get_max_liquidity() == 123


@responses.activate
def test_get_max_liquidity_raises_if_MAXLIQUIDITYRUNE_and_MAXIMUMLIQUIDITYRUNE_are_missing():
    responses.add(
        responses.GET,
        "https://thornode.thorchain.info/thorchain/mimir",
        status=200,
        json={},
    )
    with pytest.raises(Exception, match="failed to determine maximum liquidity"):
        get_max_liquidity()
