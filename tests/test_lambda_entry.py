from unittest.mock import call

import boto3
from moto import mock_s3

from thorchain_liquidity_alerts.lambda_entry import handler


def test_invoke_lambda_entry_when_max_liquidity_has_not_changed(mocker, monkeypatch):
    monkeypatch.setenv("INIT_MAX_LIQUIDITY", "101")
    mocker.patch(
        "thorchain_liquidity_alerts.lambda_entry.get_max_liquidity",
        autospec=True,
        return_value=101,
    )
    send_alert = mocker.patch("thorchain_liquidity_alerts.lambda_entry.send_alert", autospec=True)
    handler(mocker.Mock(), mocker.Mock())
    assert send_alert.called is False


def test_invoke_lambda_entry_when_max_liquidity_has_changed(mocker, monkeypatch):
    monkeypatch.setenv("INIT_MAX_LIQUIDITY", "101")
    mocker.patch(
        "thorchain_liquidity_alerts.lambda_entry.get_max_liquidity",
        autospec=True,
        return_value=102,
    )
    send_alert = mocker.patch("thorchain_liquidity_alerts.lambda_entry.send_alert", autospec=True)
    handler(mocker.Mock(), mocker.Mock())
    assert send_alert.call_args == call(
        "Max liquidity changed",
        "Max liquidity is now: 102; previous liquidity: 101",
    )


@mock_s3
def test_invoke_lambda_persists_max_liquidity(mocker, monkeypatch):
    monkeypatch.setenv("S3_BUCKET_NAME", "bucket1")
    monkeypatch.setenv("INIT_MAX_LIQUIDITY", "101")

    s3 = boto3.resource("s3", region_name="us-east-1")
    s3.create_bucket(Bucket="bucket1")

    get_max_liquidity = mocker.patch(
        "thorchain_liquidity_alerts.lambda_entry.get_max_liquidity",
        autospec=True,
    )
    send_alert = mocker.patch("thorchain_liquidity_alerts.lambda_entry.send_alert", autospec=True)
    get_max_liquidity.return_value = 102
    handler(mocker.Mock(), mocker.Mock())
    assert send_alert.called is True
    send_alert.reset_mock()

    handler(mocker.Mock(), mocker.Mock())
    assert send_alert.called is False
    send_alert.reset_mock()

    get_max_liquidity.return_value = 103
    handler(mocker.Mock(), mocker.Mock())
    assert send_alert.called is True
