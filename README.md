# Thorchain Liquidity Cap Raise Alerts

Send alerts via email, text and phone calls whenever the Thorchain liquidity cap is raised.

## Project Setup

### tox

This project uses tox to assist in orchestrating Python virtual environments and running
commands within them.

To install tox, run:

```bash
pip install tox
```

### pre-commit hooks

This project utilises pre-commit hooks to ensure newly committed code conforms to the project's
style guidelines. Install the pre-commit hooks by:

1) If pre-commit is not yet installed on your machine, run:

    ```bash
    pip install pre-commit
    ```

2) Once pre-commit is installed, install the pre-commit hooks by running:

    ```bash
    pre-commit install
    ```

## Tests

To execute the pytest tests for this application, run the following command:

```bash
tox -e test
```

To run the entire test suite (linting, mypy, etc.):

```bash
tox
```

## Deploying to AWS

Deployment to AWS is made simple using a tox command. To deploy the stack to AWS, follow these
instructions:

1) Navigate to the `cdk` command in the `tox.ini` file.
2) Note the variables used during deployments:
    - `ENVIRONMENT_NAME`
        - This variable will determine the naming of the different AWS resources belonging to
          the `TLA` stack (and the naming of the stack itself) when a deployment is performed.
        - It is important to understand the role if this environment variable. If you are
          deploying for the first time, and `ENVIRONMENT_NAME` is set to `dev`, a new
          stack and associated resources are created with `dev` appended to their names.
          The second time you run a deployment, all resources belonging to this stack will
          be updated.
        - If this project's stack has already been created with a certain `ENVIRONMENT_NAME` set,
          running a deployment with the same `ENVIRONMENT_NAME` will result in an update to that
          stack.
    - `MOBILE_NUMBERS`
        - This environment variable determines the mobile numbers that will be notified when
          the application detects that the Thorchain liquidity cap has been raised.
        - Note that the phone number will be sent a confirmation with an OTP that must be verified
          via the AWS console to begin receiving text messages.
    - `EMAIL_ADDRESSES`
        - This environment variable determines the email addresses that will be notified when the
          application detects that the Thorchain liquidity cap has been raised.
        - Note that the email address will be sent a confirmation with a link that the user
          must select in order to begin receiving alert emails.
    - `INIT_MAX_LIQUIDITY`
        - This is the current liquidity cap which can be found by inspecting the
          `mimir//MAXIMUMLIQUIDITYRUNE` value at https://thornode.thorchain.info/thorchain/mimir
    - `TWILIO_ACCOUNT_SID` and `TWILIO_AUTH_TOKEN`
        - These are your Twilio authentication values which can be found at
          https://console.twilio.com/
    - `TWILIO_FROM_NUMBER`
        - This must be an active Twilio phone number configured for voice messaging.
    - `TWILIO_TO_NUMBERS`
        - This should be a comma seperated list of phone numbers that will receive voice
          message alerts.
3) In your terminal, set the environment variables to have appropriate values:
    ```bash
    export ENVIRONMENT_NAME=<environment name>
    export MOBILE_NUMBERS=<comma separated list of mobile numbers to receive text messages>
    export EMAIL_ADDRESSES=<comma separated list of email addresses>
    export INIT_MAX_LIQUIDITY=<current thorchain liquidity cap>
    export TWILIO_ACCOUNT_SID=<twilio account sid>
    export TWILIO_AUTH_TOKEN=<twilio auth token>
    export TWILIO_FROM_NUMBER=<twilio phone number>
    export TWILIO_TO_NUMBERS=<phone numbers to receive voice alerts>
    ```
4) View a diff, describing the changes that will be made in AWS using:
    ```bash
    tox -e cdk -- diff
    ```
5) To deploy the stack to AWS, execute the following command:
    ```bash
    tox -e cdk -- deploy
    ```

### Teardown

Teardown can be done in a couple of ways. You may:
- Execute the following tox command:
   ```bash
   tox -e cdk -- destroy
   ```
   - Ensure you have set an appropriate `ENVIRONMENT_NAME` as this will determine which
     stack is destroyed.
- Navigate to the `CloudFormation` section of the AWS Console, select the stack you wish to delete
  and select `Delete`.

_Open sourced by [bitedge.com](https://bitedge.com/)._
