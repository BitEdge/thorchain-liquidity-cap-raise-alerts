from __future__ import annotations

import json
import logging

import boto3


logger = logging.getLogger()
logger.setLevel(logging.INFO)


class PersistenceClient:
    def __init__(self, bucket_name: str, file_key: str) -> None:
        self._s3_client = boto3.client("s3")
        self._bucket_name = bucket_name
        self._file_key = file_key
        self._document = self._get_document()

    def get(self, key: str, default: object = None) -> object:
        return self._document.get(key, default)

    def set(self, key: str, value: object) -> None:
        self._document[key] = value

    def commit(self) -> None:
        data = json.dumps(self._document).encode("UTF-8")
        try:
            self._s3_client.put_object(
                Bucket=self._bucket_name,
                Key=self._file_key,
                Body=data,
                ContentType="application/json",
            )
        except Exception:
            logger.exception("Failed to put object in bucket")
        else:
            logger.info("Successfully put item in bucket")

    def _get_document(self) -> dict[str, object]:
        try:
            response = self._s3_client.get_object(Bucket=self._bucket_name, Key=self._file_key)
            return json.loads(response["Body"].read())
        except Exception:
            logger.warning("Failed to retrieve file from S3")
            return {}
