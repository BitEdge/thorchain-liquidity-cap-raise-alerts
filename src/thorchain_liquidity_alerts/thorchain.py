import requests


def get_max_liquidity() -> int:
    url = "https://thornode.thorchain.info/thorchain/mimir"
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    max_liquidity = max(
        [
            data.get("mimir//MAXLIQUIDITYRUNE", 0),
            data.get("mimir//MAXIMUMLIQUIDITYRUNE", 0),
        ]
    )
    if max_liquidity == 0:
        raise Exception("failed to determine maximum liquidity")
    return max_liquidity
