import logging
import os

import boto3
from twilio.rest import Client as TwilioClient  # type: ignore


logger = logging.getLogger()


def send_alert(subject: str, message: str) -> None:
    _send_to_sns(subject, message)
    _send_to_twilio()


def _send_to_sns(subject: str, message: str) -> None:
    topic_arn = os.environ.get("ALERTS_TOPIC_ARN")

    if not topic_arn:
        logger.warning("sns ALERTS_TOPIC_ARN env var not configure")
        return

    sns = boto3.client("sns")
    try:
        sns.publish(
            TopicArn=topic_arn,
            Message=message,
            Subject=subject,
            MessageAttributes={
                "AWS.SNS.SMS.SMSType": {"DataType": "String", "StringValue": "Transactional"}
            },
        )
    except Exception:
        logger.exception("Failed to send message to topic")
    else:
        logger.info("Successfully sent message to topic")


def _send_to_twilio() -> None:
    account_sid = os.environ.get("TWILIO_ACCOUNT_SID")
    auth_token = os.environ.get("TWILIO_AUTH_TOKEN")
    if not account_sid or not auth_token:
        logger.warning("twilio env vars not configured")
        return

    twilio = TwilioClient(account_sid, auth_token)

    from_number = os.environ["TWILIO_FROM_NUMBER"]
    to_numbers = os.environ["TWILIO_TO_NUMBERS"].split(",")
    message = "<Response><Say>The thorchain liquidity cap has changed.</Say></Response>"
    for to_number in to_numbers:
        call = twilio.calls.create(twiml=message, from_=from_number, to=to_number)
        logger.info(f"sent voice alert to {to_number} (sid={call.sid})")
