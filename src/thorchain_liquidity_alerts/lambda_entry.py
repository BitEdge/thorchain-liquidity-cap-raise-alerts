import logging
import os

from thorchain_liquidity_alerts.alert import send_alert
from thorchain_liquidity_alerts.persistence import PersistenceClient
from thorchain_liquidity_alerts.thorchain import get_max_liquidity


logger = logging.getLogger()
logger.setLevel(logging.INFO)

S3_FILE_KEY = "state.json"


def handler(event: object, context: object) -> None:
    logger.info("started")
    max_liquidity = get_max_liquidity()
    db = PersistenceClient(os.environ["S3_BUCKET_NAME"], S3_FILE_KEY)
    prev_liquidity = db.get("max_liquidity", int(os.environ["INIT_MAX_LIQUIDITY"]))
    logger.info(f"Max liquidity is now: {max_liquidity}; previous liquidity: {prev_liquidity}")
    if max_liquidity != prev_liquidity:
        db.set("max_liquidity", max_liquidity)
        db.commit()
        send_alert(
            "Max liquidity changed",
            f"Max liquidity is now: {max_liquidity}; previous liquidity: {prev_liquidity}",
        )
    logger.info("completed")
