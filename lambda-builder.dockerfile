FROM lambci/lambda:20201122-build-python3.8
ENV PIP_NO_CACHE_DIR=1
WORKDIR /app/
RUN mkdir /build/
COPY requirements.txt .
RUN pip install -r requirements.txt -t /build/
COPY setup.py .
COPY src ./src
RUN pip install . -t /build/
CMD ["cp", "-R", "/build/.", "/asset-output/"]
