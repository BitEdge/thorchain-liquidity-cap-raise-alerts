import os
from pathlib import Path

import aws_cdk.aws_events as aws_events
import aws_cdk.aws_events_targets as aws_events_targets
import aws_cdk.aws_lambda as aws_lambda
import aws_cdk.aws_logs as aws_logs
import aws_cdk.aws_s3 as aws_s3
import aws_cdk.aws_sns as aws_sns
import aws_cdk.aws_sns_subscriptions as aws_sns_subscriptions
import aws_cdk.core as core


ENVIRONMENT_NAME = os.environ["ENVIRONMENT_NAME"]

MOBILE_NUMBERS = os.environ["MOBILE_NUMBERS"].split(",")
EMAIL_ADDRESSES = os.environ["EMAIL_ADDRESSES"].split(",")
INIT_MAX_LIQUIDITY = os.environ.get("INIT_MAX_LIQUIDITY", "550000000000000")


class ThorchainLiquidityAlertsStack(core.Stack):
    def __init__(self, scope: core.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        build_image = core.BundlingDockerImage.from_asset(
            path=str(Path.cwd()), file="lambda-builder.dockerfile"
        )

        # topic used to send email and SMS based alerts when liquidity cap is lifted
        topic = aws_sns.Topic(
            self,
            f"TLATopic-{ENVIRONMENT_NAME}",
            topic_name=f"TLATopic-{ENVIRONMENT_NAME}",
            display_name="Thorchain liquidity cap alert",
        )

        for email_address in EMAIL_ADDRESSES:
            email_subscription = aws_sns_subscriptions.EmailSubscription(email_address)
            topic.add_subscription(email_subscription)

        for mobile_number in MOBILE_NUMBERS:
            sms_subscription = aws_sns_subscriptions.SmsSubscription(mobile_number)
            topic.add_subscription(sms_subscription)

        # s3 bucket used to track which tweets we've already alerted about
        bucket = aws_s3.Bucket(self, f"TLABucket-{ENVIRONMENT_NAME}")

        # lambda that periodically checks if the liquidity cap has been raised
        tla_lambda = aws_lambda.Function(
            self,
            f"TLALambda-{ENVIRONMENT_NAME}",
            code=aws_lambda.Code.from_asset(
                path=".",
                bundling=core.BundlingOptions(image=build_image),  # type: ignore
                asset_hash_type=core.AssetHashType.OUTPUT,
            ),
            handler="thorchain_liquidity_alerts.lambda_entry.handler",
            function_name=f"TLALambda-{ENVIRONMENT_NAME}",
            runtime=aws_lambda.Runtime.PYTHON_3_8,
            timeout=core.Duration.seconds(15),
            log_retention=aws_logs.RetentionDays.ONE_MONTH,
            environment={
                "ALERTS_TOPIC_ARN": topic.topic_arn,
                "S3_BUCKET_NAME": bucket.bucket_name,
                "INIT_MAX_LIQUIDITY": INIT_MAX_LIQUIDITY,
                "TWILIO_ACCOUNT_SID": os.environ["TWILIO_ACCOUNT_SID"],
                "TWILIO_AUTH_TOKEN": os.environ["TWILIO_AUTH_TOKEN"],
                "TWILIO_FROM_NUMBER": os.environ["TWILIO_FROM_NUMBER"],
                "TWILIO_TO_NUMBERS": os.environ["TWILIO_TO_NUMBERS"],
            },
        )

        # schedule the lambda to run periodically (once each minute)
        rule = aws_events.Rule(
            self,
            f"TLARule-{ENVIRONMENT_NAME}",
            schedule=aws_events.Schedule.cron(minute="*", hour="*", day="*", month="*", year="*"),
        )
        rule.add_target(aws_events_targets.LambdaFunction(tla_lambda))

        topic.grant_publish(tla_lambda)
        bucket.grant_put(tla_lambda)
        bucket.grant_read_write(tla_lambda)


if __name__ == "__main__":
    app = core.App()
    ThorchainLiquidityAlertsStack(app, f"TLA-{ENVIRONMENT_NAME}")
    app.synth()
