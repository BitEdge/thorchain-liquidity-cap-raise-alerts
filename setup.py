from setuptools import setup  # type: ignore


setup(
    name="thorchain_liquidity_alerts",
    package_dir={"": "src"},
    packages=["thorchain_liquidity_alerts"],
)
